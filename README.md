# airpage
## 概述:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;这是一个基于airtest的仅适用Windows平台的二次开发库，主要抽象了[页面]这一模型，在此基础上融合了行为树、文件-数据IO等功能，进一步简化了脚本开发周期，强化了脚本的自洽稳定性，融合其他模块以实现更多脚本功能。

<a href="https://gitee.com/eagle-s_baby/airpage/blob/master/readme/readme.md">转到readme.md</a>
