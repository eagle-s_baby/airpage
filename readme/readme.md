<div align="center">
    <h1>AirPage使用手册</h1>
</div>
<style>
pre {
  font-size: 16px;
}
def {
  color: #FFB90F;
}
deco {
  color: #FFB90F;
}
note_long {
  color: #8FBC8F;
}
note {
  color: #8B8B7A;
}
</style>

> ## 概述:  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;这是一个基于airtest的仅适用Windows平台的二次开发库，主要抽象了<i>[页面]</i>这一模型，在此基础上融合了行为树、文件-数据IO等功能，进一步简化了脚本开发周期，强化了脚本的自洽稳定性，融合其他模块以实现更多脚本功能。  

> <details><summary style="font-size: 24px">安装与环境配置:</summary>
> 1. 安装python3.9 (尚未在其他python版本下进行测试, 为了防止bug, 暂仅支持python3.9)<br>
> 2. 安装AirtestIDE, 安装方法略(需要在其'选项'->'设置'->'自定义python.exe'中选择python3.9)<br>
> 3. pip install airpage<br>
> <pre>安装过程中可能抛出numpy兼容性Error, 可以直接忽略, 不影响正常使用。</pre>
> *4. 安装Graphviz(用于绘制行为树, 没有需求的可以不安装)<a href="https://zhuanlan.zhihu.com/p/268532582">Graphviz安装教程</a><br>
> 
> 完成上述步骤后，在cmd中输入ap_version回车，若正常显示版本号，则安装成功。
> <img src="cmd.png">
> </details>

> <details><summary style="font-size: 24px">Page对象:</summary>
> <p style="color: #C0C0C0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;经过对脚本行为的简单分析，私以为整个脚本可以拆分为若干子脚本，划分依据是执行某操作时<i>[画面]</i>中是否出现属于某个<i>[页面]</i>的标志。<i>[页面]</i>即总是出现某种标志的画面的集合，判断<i>[画面]</i>是否为某个<i>[页面]</i>的方法就是判断画面中是否有对应的某种标志。从一个<i>[页面]</i>到另一个<i>[页面]</i>的一段操作成为<i>[转移]</i>，这将允许程序从一个<i>[页面]</i>转移到任意可达的<i>[页面]</i>; 然后将具体的脚本逻辑安放在对应的<i>[页面]</i>上, 在执行这段脚本时先<i>[转移]</i>到该<i>[页面]</i>，然后执行，必能裨补缺漏，有所广益。<br></p>
> <p style="color: #C0C0C0"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下文中将<i>[页面]</i>称为Page, <i>[画面]</i>称为Screen, <i>[转移]</i>称为Transifer, 将绑定到某一Page上的一段逻辑脚本称为Task.</b></p>
>
>> <details><summary style="font-size: 20px">构造方法:</summary>
>> <pre style="font-size: 16px">Page(name:str, identity:Any)</pre>
>> 构造一个抽象Page的实例<br>
>> :<i>param</i> <b>name</b>: 该Page的名称(在同一个环境下，Page对于每个名称都是对应的单例)<br>
>> :<i>param</i> <b>identity</b>: 该Page的验证标识(稍后细讲，目前可以理解为需要传入Template) 当需要判断Screen是否为此Page时，会通过此Identity进行比对得出结果<br>
>> <br>
>> <b>@Example:</b>
>> <pre style="font-size: 16px"><code>
>> page_main  = Page("main",  <img src='tpl1678786317899.png' width=100>)
>> page_level = Page("level", <img src='tpl1678786337822.png' width=100>)
>> <note_long>"""
>> <details> 
>>     <summary>截屏图片</summary>
>>     <img src="snapshot0.png" width="600">
>>     <img src="snapshot1.png" width="600">
>> </details>"""</note_long></code></pre></details>
> ------
>> <details><summary style="font-size: 20px">页面连接:</summary>
>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;页面连接可以让从任意Page到任意Page成为可能。目前有多种方式创建两个页面的连接, 首先按照airtest的方式创建两个操作
>> <pre><def>def</def> main2level() -> bool:
>>     touch(<img src='tpl1678786317899.png' width=100>)
>>     <def>return True</def>
>> 
>> <def>def</def> general_back() -> bool:
>>     touch(<img src='tpl1678786326892.png' width=30>)
>>     <def>return True</def>
>> </pre>
>> <b>方式1:</b>
>> 使用transfer装饰器
>> <pre style="font-size: 16px">
>> <def>@</def>transfer('main', 'level')<details> 
>>     <summary><def>def</def> main2level() -> bool:</summary>
>>     touch(<img src='tpl1678786317899.png' width=100>)
>>     <def>return True</def>
>> </details>
>> <deco>@</deco>transfer('level', 'main')<details> 
>>     <summary><def>def</def> general_back() -> bool:</summary>
>>     touch(<img src='tpl1678786326892.png' width=30>)
>>     <def>return True</def>
>> </details>
>> <note_long>"""
>> 一个函数可以接受多个transfer的装饰
>> <details> <summary>比如:</summary>
>> <deco>@</deco>transfer('level', 'main')
>> <deco>@</deco>transfer('build', 'main')
>> <deco>@</deco>transfer('dorm', 'main')
>> <deco>@</deco>transfer('task', 'main')<details> 
>>     <summary><def>def</def> general_back() -> bool:</summary>
>>     touch(<img src='tpl1678786317899.png' width=100>)
>>     <def>return True</def>
>> </details></details>"""</note_long></pre>
>> <b>方法2:</b>
>> 使用Page.LinkWith方法
>><pre style="font-size: 16px">
>> <details><summary><def>def</def> main2level() -> bool:</summary>
>>     touch(<img src='tpl1678786317899.png' width=100>)
>>     <def>return True</def>
>> </details><details> 
>>     <summary><def>def</def> general_back() -> bool:</summary>
>>     touch(<img src='tpl1678786326892.png' width=30>)
>>     <def>return True</def>
>> </details>Page.LinkWith('main', 'level', main2level, general_back)<details><summary><note># 等效于下面的写法</note></summary><note># page_main.link('level', main2level)</note>
>> <note># page_level.link('main', general_back)</note></details></pre>
>> 创建好连接后你可以使用下面的方法从一个Page移动到任意可以抵达的Page
>> <pre>Page.Transit(target_page:str|Page)</pre>
>> @Example:
>> <pre>Page.Transit('level')</pre></details>
> ------
>> <details><summary style="font-size: 20px">页面任务</summary>
>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;页面任务指在具有某个Page标志特征的Screen上执行的一段负责实现业务逻辑脚本, 和创建页面链接相似，添加页面任务也有多种方法。首先创建一段airtest操作
>> <pre><code class="python"><def>def</def> purr() -> bool:  <note># 让你的看板娘咕噜咕噜~</note>
>>     touch(ap_pos(-0.2, 0))  
>>     <note># ap_pos将airtest的中心相对坐标 转换为 在当前分辨率下的相对坐标</note>
>>     <note># 详见airtest的中心相对坐标系</note>
>>     <def>return True</def>
>> </code></pre>
>> <b>方式1:</b>
>> 使用page进行装饰
>> <pre>
>> <def>@</def>page('main')<details> 
>>     <summary><def>def</def> purr() -> bool:</summary>
>>     touch(ap_pos(-0.2, 0))  
>>     <def>return True</def>
>> </details></pre>
>> <b>方式2:</b>
>> 使用Page.LoginTask方法
>> <pre>
>> <details> 
>>     <summary><def>def</def> purr() -> bool:</summary>
>>     touch(ap_pos(-0.2, 0))  
>>     <def>return True</def>
>> </details>Page.LoginTask('main', purr)
>> <note># 等效于下面的写法</note>
>> <note># page_main.login(purr)</note>
>> </pre>
>> 添加好Task后，你可以在Screen处于任意Page时直接调用purr。 程序会先将Screen转移至任务所在的Page，然后执行Task
>> <pre>purr(*args, **kwargs)</pre>
>> 另一种方式是通过Page.DoTask调用
>> <pre>Page.DoTask('main', 'purr', *args, **kwargs)
>> <note># 等效于下面的写法</note>
>> <note># page_main.do('purr', *args, **kwargs)</note>
>> </pre>
>> </details>
> 更多有关Page的方法详见API手册.
> </details>

> <details><summary style="font-size: 24px">Identity对象:</summary>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;回顾Page的构造函数的第二个参数，你可以传入Template，但它还可以用于更复杂的标志判断。比如某个Page的标志需要同时满足多个Template; 或满足多个Template中的任意一个; 或更复杂的逻辑组合。<br>
>
> 这里需要先引入逻辑组合, 即and or not对应的Identity写法:
> <pre>
> <note># 假设你有两个Template: tpl1 和 tpl2</note> 
> tpl1 = ...
> tpl2 = ...
>
> <note># 可以自由组合</note>
> (tpl1, tpl2)  <note># tpl1 and tpl2</note> 
> [tpl1, tpl2]  <note># tpl1 or tpl2</note> 
> Not(tpl1)  <note># not tpl1</note> </pre>
> <note># 我相信这里看一眼就懂了</note><br>
> @Example
> <pre>
> <note># 比如Page中定义的level界面就可以使用组合标志来提升稳定性</note>
> page_level = Page("level", (<img src='tpl1678786337822.png' width=100>, <img src='tpl1679157854538.png' width="100">))
> <note_long>"""
> <details> 
>     <summary>截屏图片</summary>
>     <img src="snapshot2.png" width="600">
> </details>"""</note_long></pre>
> 更多有关Identity的方法详见API手册.
> </details>

> <details><summary style="font-size: 24px">BTree:</summary>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;除了创建阻塞型的连续脚本，你还可以将你的脚本打包为行为树。airpage的行为树主要继承了py_trees中的Behaviour类，其详细原理请自行学习，这里不再赘述。<br>
>
>> 下面先看看如何构造基本行为树叶子节点:<br>
>> <pre><def>def</def> do_something(sth):
>>   print(f"done {sth}")
>>   <def>return True </def>
>>   <note># 自定义的行为树函数必须返回SUCCESS|FAILURE|RUNNING中的一种，否则会抛出异常</note>
>>   <note># 对应关系: SUCCESS - True | FAILURE - False | RUNNING - ...</note>
>> <def>def</def> if_something(sth):
>>   <def>return</def> bool(sth)</pre>
>> 现在我要把do_something这个函数打包为行为树节点
>> <pre>a = action(do_something, "第一件事请")
>> print("行为树的执行结果为: ", a())</pre>
>> <details><summary>下面的表格列出了airpage提供的叶子节点</summary>
>> <table border="1" width="500px" cellspacing="10">
>> <tr>
>>   <th align="center">节点名称</th>
>>   <th align="center">节点描述</th>
>>   <th align="center">Paramters</th>
>> </tr>
>> <tr>
>>   <td>action</td>
>>   <td>将任意函数与其参数打包为一个Action节点</td>
>>   <td>fn, *args, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>condtion</td>
>>   <td>将任意函数与其参数打包为一个Condition节点(不允许返回RUNNING)</td>
>>   <td>fn, *args, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>action_touch</td>
>>   <td>打包touch函数(参数与被打包的touch函数一致, 下同)</td>
>>   <td>v, *, times=1, match=True, delay=None, timeout=ST.FIND_TIMEOUT, threshold=None, interval=0.5, snapshot=True, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>action_tickle</td>
>>   <td>与action_touch类似，但是调整了timeout的时间</td>
>>   <td>v, *, times=1, match=True, delay=None, timeout=ST.FIND_TIMEOUT_TMP, threshold=None, interval=0.5, snapshot=True, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>action_swipe</td>
>>   <td>打包swipe函数。</td>
>>   <td>v1, v2=None, *, vector=None, match=True, duration=1.0, delay=None, timeout=ST.FIND_TIMEOUT_TMP, threshold=None, interval=0.5, snapshot=True, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>action_sleep</td>
>>   <td>打包sleep函数。</td>
>>   <td>duration=1.0</td>
>> </tr>
>> <tr>
>>   <td>action_pinch</td>
>>   <td>打包pinch函数。</td>
>>   <td>in_or_out='in', center=None, percent=0.5, delay=None</td>
>> </tr>
>> <tr>
>>   <td>action_text</td>
>>   <td>打包text函数。</td>
>>   <td>_text, *, enter=True, delay=None, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>action_keyevent</td>
>>   <td>打包keyevent函数。</td>
>>   <td>keyname, *, delay=None, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>action_keyboard</td>
>>   <td>打包keyboard函数。</td>
>>   <td>*key_code, internal=0.1, delay=None</td>
>> </tr>
>> <tr>
>>   <td>action_label</td>
>>   <td>打包keyboard函数。</td>
>>   <td>desc</td>
>> </tr>
>> <tr>
>>   <td>action_info</td>
>>   <td>打包info函数。</td>
>>   <td>*args</td>
>> </tr>
>> <tr>
>>   <td>action_warn</td>
>>   <td>打包warn函数。</td>
>>   <td>*args</td>
>> </tr>
>> <tr>
>>   <td>action_error</td>
>>   <td>打包error函数。</td>
>>   <td>err_type:str, *args</td>
>> </tr>
>> <tr>
>>   <td>action_transit</td>
>>   <td>打包Page.Transit函数。</td>
>>   <td>*args</td>
>> </tr>
>> <tr>
>>   <td>action_dotask</td>
>>   <td>打包Page.DoTask函数。</td>
>>   <td>*args</td>
>> </tr>
>> <tr>
>>   <td>condition_wait</td>
>>   <td>打包wait函数。</td>
>>   <td>querys, *, timeout=ST.FIND_TIMEOUT, threshold=None, interval=0.5, snapshot=True</td>
>> </tr>
>> <tr>
>>   <td>condition_exists</td>
>>   <td>与condition_wait类似，但是调整了timeout的时间</td>
>>   <td>querys, *, timeout=ST.FIND_TIMEOUT_TMP, threshold=None, interval=0.5, snapshot=True</td>
>> </tr>
>> <tr>
>>   <td>condition_identity</td>
>>   <td>打包Identity.Check方法。</td>
>>   <td>identity, *, timeout=ST.FIND_TIMEOUT, threshold=None, interval=0.5, snapshot=True</td>
>> </tr>
>> <tr>
>>   <td>condition_page</td>
>>   <td>打包Page.Check方法。</td>
>>   <td>page, *, timeout=ST.FIND_TIMEOUT, threshold=None, interval=0.5, snapshot=True</td>
>> </tr>
>> </table></details>
> ------
>> 下面来看看行为树的组合节点:
>> <pre><note># 下面的代码实现了if xxx: do(xxx)这样的逻辑</note>
>> seq = sequence(
>>     condition(if_something, True),
>>     action(do_something, "可能的事请"),
>> )
>>
>> print("行为树的执行结果为: ", seq())</pre>
>> <details><summary>下面的表格列出了airpage提供的组合节点</summary>
>> <table border="1" width="500px" cellspacing="10">
>> <tr>
>>   <th align="center">节点名称</th>
>>   <th align="center">节点描述</th>
>>   <th align="center">Paramters</th>
>> </tr>
>> <tr>
>>   <td>sequence</td>
>>   <td>依次执行子节点组, 直到某个子节点返回RUNNING时返回RUNNING。如果所有子节点都返回了SUCCESS，那么返回SUCCESS</td>
>>   <td>*nodes, name=None, pack=False</td>
>> </tr>
>> <tr>
>>   <td>fallback</td>
>>   <td>依次执行子节点组, 直到某个子节点返回RUNNING时返回RUNNING。如果所有子节点都返回了FAILURE，那么返回FAILURE</td>
>>   <td>*nodes, name=None, pack=False</td>
>> </tr>
>> <tr>
>>   <td>parallel</td>
>>   <td>并行运行所有子节点，等待所有子节点结束。返回结果与policy有关.</td>
>>   <td>*nodes, name=None, pack=False, policy='and', thread_num=cpu_count+1</td>
>> </tr>
>> </table></details>
> ------
>> 下面来看看行为树的装饰节点:
>> <pre>t1 = repeater(action(do_something, "重要的事情说三遍"), times=3)
>> t2 = repeater(  <note># 如果装饰节点传入多个子节点，那么会默认使用sequence将其组合</note>
>>     action(do_something, "不重复的任务1"),
>>     inverter(action(do_something, "不重复的任务2")),
>>     until=False,  <note># 当出现FAILURE时终止循环</note>
>> )
>> 
>> print("行为树t1的执行结果为: ", t1())
>> print("行为树t2的执行结果为: ", t2())</pre>
>> <details><summary>下面的表格列出了airpage提供的装饰节点</summary>
>> <table border="1" width="500px" cellspacing="10">
>> <tr>
>>   <th align="center">节点名称</th>
>>   <th align="center">节点描述</th>
>>   <th align="center">Paramters</th>
>> </tr>
>> <tr>
>>   <td>repeater</td>
>>   <td>重复循环子节点n次后返回SUCCESS。如果定义了until, 那么在耗尽循环次数n前满足until时返回SUCCESS, 否则返回FAILURE。子节点返回RUNNING时也返回RUNNING(只有返回非RUNNING时才视作完成了一次循环)</td>
>>   <td>*nodes, name=None, pack=False</td>
>> </tr>
>> <tr>
>>   <td>succeedor</td>
>>   <td>不论子节点返回了什么，总是向上一节点返回SUCCESS</td>
>>   <td>*nodes, name=None, pack=False</td>
>> </tr>
>> <tr>
>>   <td>inverter</td>
>>   <td>对子节点的结果进行取反。子节点返回RUNNING时也返回RUNNING</td>
>>   <td>*nodes, name=None, pack=False</td>
>> </tr>
>> <tr>
>>   <td>validator</td>
>>   <td>在执行子节点前先验证条件函数的返回值. 如果条件函数返回True，则返回子节点的结果，否则返回False</td>
>>   <td>condition_fn:typing.Callable, *nodes,name=None, pack=False, blackboard_keys:typing.List[str]=None, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>timeouter</td>
>>   <td>带时限执行子节点。如果超时将尝试提前结束子节点并返回FAILURE，否则返回子节点的返回值</td>
>>   <td>*nodes, name=None, pack=False, duration=5.0, **kwargs</td>
>> </tr>
>> <tr>
>>   <td>counter</td>
>>   <td>记录子节点各种状态出现的次数, 无法作为root节点。直接调用此对象将返回一段报告文本以显示子节点运行的情况</td>
>>   <td>*nodes, name=None, pack=False</td>
>> </tr>
>> <tr>
>>   <td>oneshoter</td>
>>   <td>一直执行子节点，直到子节点返回非RUNNING的情况才算结束。返回值取决于policy参数的值</td>
>>   <td>*nodes, name=None, pack=False, policy='pass', **kwargs</td>
>> </tr>
>> <tr>
>>   <td>reflictor</td>
>>   <td>将子节点的某种返回值映射为另一返回值(x->y)</td>
>>   <td>*nodes, name=None, pack=False, x=RUNNING, y=FAILURE, **kwargs</td>
>> </tr>
>> </table></details>
> ------
>> 最后来看看如何渲染airpage中的行为树
>> <pre><note_long>""" 
>> 下面的代码实现了:
>> if xxx: 
>>     for i in range(3):
>>         do(xxx)
>> else:
>>     do(xxx)
>> 这样的逻辑
>> """</note_long>
>> t = fallback(
>>     sequence(
>>         condition(if_something, "条件1"),
>>         repeater(action(do_something, "事请1"), times=3),
>>     ),
>>     action(do_something, "事请2")
>> )
>>
>> t.render(open=True)  <note># 如果询问如何打开.svg文件时，选择edge即可</note></pre>
>> <details><summary>渲染图</summary><img src="tree.png"></details>
>> 
>> 
>
> </details>

> <details><summary style="font-size: 24px">快速开始</summary>
> 下面以在azur-line 8-4刷moe为例，借助python3.9和AirtestIDE从头开始创建脚本<br>
>
>> ### 0.新建工程<br>
>> 打开cmd, 使用'ap_new PROJECT_PATH'来创建.air工程<br>
>> <pre>ap_new azur.air</pre>
>> 创建好的新工程包含3个文件: azur.py(主脚本入口)、 page.py(自定义page)、 backend.py(自定义函数)<br>
>> <details><summary>将这三个文件依次拖入AirtestIDE中, 其中已有的初始代码如下</summary>
>> <img src="azur.py.png">
>> <img src="page.py.png">
>> <img src="backend.py.png"></details>
>> <details><summary>使用AirtestIDE绑定到逍遥模拟器(最好固定窗口尺寸和分辨率, 比如我用的是960x540)</summary>
>> <img src="ide_device_0.png"></details>
>> 启动脚本时注意只能从主脚本启动，否则会抛出异常
> ---
>> ### 1.启动游戏<br>
>> <details><summary>根据airtest的用法先在主脚本中搓一段简单脚本</summary>
>> <img src="code_startgame.png"></details>
>> <details><summary>自己测试，确定其可以正常运行</summary>
>> <img src="ide_device_1.png"></details>
>> <b>* 观察到游戏启动后，模拟器顶部任务栏会显示azur的的logo，以此可以判断游戏是否启动, 并将其作为Page:'shut'的标志:<br>
>> * 然后直接把'出击'作为Page:'main'的标志: <br>
>> * 最后把刚才搓的脚本转为行为树后，作为由shut->main的转移函数(可以没有main->shut):</b>
>> <details><summary>根据上述操作修改page.py, 如图</summary>
>> <img src="page_startgame.png"></details>
>> 在主脚本中使用<pre>Page.Transit('main')</pre>来测试代码有无正常工作
> ---
>> ### 2.刷爆8-4<br>
>> <details><summary>定义两个Page:'level0'和'level1'来分别描述azur的两个选关页面，如下图:</summary>
>> <img src="level0.png">
>> <img src="level1.png">
>> <img src="page_level0_level1.png">
>> <img src="page_level0_level1_trans.png"></details>
>> <details><summary>接下来搓一个在'level1'中切换到指定章节的复位脚本(先切到第1章，然后再切到指定章节):</summary>
>> <img src="level1_reset.png"></details>
>> 刷爆8-4只需要先定位到第8章，然后利用章回模式自动刷即可。<br>
>> 然后仿照‘1.启动游戏’中的方法，先在主脚本中搓打8-4的脚本。<br>
>> <details><summary>确定ok后把这段脚本转为行为树，在page.py将其添加到'level1'下:</summary>
>> <img src="level1_8_4.png"></details>
>> 最后在主脚本指定主逻辑即可:
>> <pre>script = repeater(
>>      action_dotask('level1', '8-4'),
>>      times=30
>> )
>> script()  # 执行脚本
>> </pre>
>
> 这个简单的脚本还有许多缺陷，比如如果船坞装满后还需要进行退役; 你的老婆一直出击心情会不好; 功能还很单一等等。在这里仅起到引导你入门的作用。
> </details>

